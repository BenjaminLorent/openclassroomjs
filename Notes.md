// déclarer variable
let numberOfCats = 2;
let numberOfDogs = 4;

// déclarer constante
const nombrePostParPage = 20;
nombrePostParPage = 30; // Retournera une erreur dans la console car on ne peut plus changer sa valeur

// déclarer un objet
let episode = {
    title: 'Mon episode',
    duration: 45,
    hasBeenWatched: true
    };

    // Récupération de valeur depuis un objet notation dot .
    let episode = {
        title: 'Dark Beginnings',
        duration: 45,
        hasBeenWatched: false
      };
      
      // Create variables here
      // =====================================
      let episodeTitle = episode.title;
      let episodeDuration = episode.duration
      let episodeHasBeenWatched = episode.hasBeenWatched

// Les Classes

class Episode {
    constructor(title, duration, hasBeenWatched){
      this.title = title;
      this.duration = duration;
      this.hasBeenWatched = hasBeenWatched;
    }
  }
  let firstEpisode = new Episode("EPISODE1", 45, true);
  let secondEpisode = new Episode("EPISODE2", 50, false);
  let thirdEpisode = new Episode("EPISODE3", 47, false);
 
  // Conditions

  if (numberOfGuests == numberOfSeats) {
    // tous les sièges sont occupés
    } else if (numberOfGuests < numberOfSeats) {
    // autoriser plus d'invités
    } else {
    // ne pas autoriser de nouveaux invités
    }
let userLoggedIn = true;
let UserHasPremiumAccount = true;
let userHasMegaPremiumAccount = false;

userLoggedIn && userHasPremiumAccount; // true
userLoggedIn && userHasMegaPremiumAccount; // false

userLoggedIn || userHasPremiumAccount; // true
userLoggedIn || userHasMegaPremiumAccount; // true

!userLoggedIn; // false
!userHasMegaPremiumAccount; // true


## Odre de création des modules

- models -> contient le model de notre objet
- routes -> chemin des routes de l'api