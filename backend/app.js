const express = require('express');

const mongoose = require('mongoose');
//appel du router stuffRoutes qui vient du dossier routes/stuff.js
const stuffRoutes = require('./routes/stuff');
const userRoutes = require('./routes/user');
const app = express();

// connection à la mongo

mongoose.connect('mongodb+srv://admin:CoinCoin59@cluster0.0k9mu.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
  { useNewUrlParser: true,
    useUnifiedTopology: true })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));
//methode qui permet à expresse de récuperer le body d'un json
app.use(express.json());

// middleware global permettant d'ouvrir l'api grace aux headers evite le CORS (Cross Origin Resource Sharing)
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
  });

  //Utilisation du router stuffRoutes
  app.use('/api/stuff', stuffRoutes);
  app.use('/api/auth', userRoutes);

  //export du module app
module.exports = app;