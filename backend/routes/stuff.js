const express = require('express');
const router = express.Router();

// appel du controller stuff situé dans le dossier ../controllers/stuff
const stuffCtrl = require('../controllers/stuff');

//déclaration des routes
router.get('/', stuffCtrl.getAllStuff);
router.post('/', stuffCtrl.createThing);
router.get('/:id', stuffCtrl.getOneThing);
router.put('/:id', stuffCtrl.modifyThing);
router.delete('/:id', stuffCtrl.deleteThing);

//export du module router
module.exports = router;