//model utilisateur

const mongoose = require("mongoose")

const uniqueValidator = require('mongoose-unique-validator');
//Schema de l'utilisateur
const userSchema = mongoose.Schema({
    //unique rend impossible de créer 2 user avec le même mail il est préférable d'installer
    //mongoose-unique-validator pour eviter des erreur 'npm install mongoose-unique-validator'
    email: {type: String, required: true, unique: true }, 
    password: {type: String, required: true}
});

//appel du plugin uniqueValidator dans noter model User
userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', userSchema);